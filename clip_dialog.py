#!/usr/bin/env python3

# Copyright (C) 2020 Harry Sentonbury
# GNU General Public License v3.0

from PyQt5.Qt import Qt
from PyQt5.QtWidgets import (QWidget, QLabel, QPushButton,
                             QVBoxLayout, QSlider, QApplication)

from PyQt5.QtGui import QIcon
import sys


class ClipDialog(QWidget):
    def __init__(self, clips, folds, phlazes, phlaze_speed, phlaze_loops,
                 phlaze_depth):
        self.clips = clips
        self.folds = folds
        self.phlazes = phlazes
        self.phlaze_speed = phlaze_speed
        self.phlaze_loops = phlaze_loops
        self.phlaze_depth = phlaze_depth

        super().__init__()

        def set_phlazes():
            self.phlazes[0] = not self.phlazes[0]
            if self.phlazes[0]:
                self.phlaze_button.setText("Phlaze On")
            else:
                self.phlaze_button.setText("Phlaze Off")


        def set_close():
            self.clips[0] = self.low_slider.value() * 0.01
            self.clips[1] = self.high_slider.value() * 0.01
            self.folds[0] = self.low_fold_slider.value() * 0.01
            self.folds[1] = self.high_fold_slider.value() * 0.01
            self.phlaze_speed[0] = self.phlaze_slider.value() * 0.01
            self.phlaze_depth[0] = self.phlaze_depth_slider.value()
            self.phlaze_loops[0] = self.phlaze_loops_slider.value()
            self.close()

        def changed_low():
            val = self.low_slider.value()
            self.lval_label.setText(str('{:3.2f}'.format(val * 0.01)))
            self.lval_label.adjustSize()
            if val > self.high_slider.value():
                self.high_slider.setValue(val)

        def changed_high():
            val = self.high_slider.value()
            self.hval_label.setText(str('{:3.2f}'.format(val * 0.01)))
            self.hval_label.adjustSize()
            if val < self.low_slider.value():
                self.low_slider.setValue(val)

        def changed_fold_low():
            val = self.low_fold_slider.value()
            self.lval_fold_label.setText(str('{:3.2f}'.format(val * 0.01)))
            self.lval_fold_label.adjustSize()
            if val > self.high_fold_slider.value():
                self.high_fold_slider.setValue(val)

        def changed_fold_high():
            val = self.high_fold_slider.value()
            self.hval_fold_label.setText(str('{:3.2f}'.format(val * 0.01)))
            self.hval_fold_label.adjustSize()
            if val < self.low_fold_slider.value():
                self.low_fold_slider.setValue(val)

        def changed_phlaze():
            val = self.phlaze_slider.value()
            self.phlaze_val_label.setText(str('{:3.2f}'.format(val * 0.01)))
            self.phlaze_val_label.adjustSize()

        def changed_phlaze_depth():
            val = self.phlaze_depth_slider.value()
            self.phlaze_depth_val_label.setText(str('{:3.2f}'.format(val)))
            self.phlaze_depth_val_label.adjustSize()

        def changed_phlaze_loops():
            val = self.phlaze_loops_slider.value()
            self.phlaze_loops_val_label.setText(str(val))
            self.phlaze_loops_val_label.adjustSize()


        self.setGeometry(200, 500, 600, 430)
        self.setWindowTitle("LFO Clip Values")
        self.setWindowIcon(QIcon('images/scope-icon.jpg'))
        self.setObjectName("clip_win")

        self.close_button = QPushButton("Set and Close", self)
        self.close_button.setMinimumWidth(130)
        self.close_button.move(400, 330)
        self.close_button.clicked.connect(set_close)

        self.high_labal = QLabel(self)
        self.high_labal.setText("High Clip")
        self.high_labal.move(20, 20)

        self.set_high = int(self.clips[1] * 100)
        self.high_slider = QSlider(Qt.Horizontal, self)
        self.high_slider.setMinimumWidth(250)
        self.high_slider.setMinimum(-100)
        self.high_slider.setMaximum(300)
        self.high_slider.setValue(self.set_high)
        self.high_slider.setSingleStep(1)
        self.high_slider.move(110, 20)
        self.high_slider.valueChanged.connect(changed_high)

        self.hval_label = QLabel(self)
        self.hval_label.setText(str("{:3.2f}".format(self.set_high * 0.01)))
        self.hval_label.move(370, 20)

        self.low_labal = QLabel(self)
        self.low_labal.setText("Low Clip")
        self.low_labal.move(20, 60)

        self.set_low = int(self.clips[0] * 100)
        self.low_slider = QSlider(Qt.Horizontal, self)
        self.low_slider.setMinimumWidth(250)
        self.low_slider.setMinimum(-100)
        self.low_slider.setMaximum(300)
        self.low_slider.setValue(self.set_low)
        self.low_slider.setSingleStep(1)
        self.low_slider.move(110, 60)
        self.low_slider.valueChanged.connect(changed_low)

        self.lval_label = QLabel(self)
        self.lval_label.setText(str("{:3.2f}".format(self.set_low * 0.01)))
        self.lval_label.move(370, 60)

        self.high_fold_labal = QLabel(self)
        self.high_fold_labal.setText("High Fold")
        self.high_fold_labal.move(20, 110)

        self.set_fold_high = int(self.folds[1] * 100)
        self.high_fold_slider = QSlider(Qt.Horizontal, self)
        self.high_fold_slider.setMinimumWidth(250)
        self.high_fold_slider.setMinimum(-100)
        self.high_fold_slider.setMaximum(100)
        self.high_fold_slider.setValue(self.set_fold_high)
        self.high_fold_slider.setSingleStep(1)
        self.high_fold_slider.move(110, 110)
        self.high_fold_slider.valueChanged.connect(changed_fold_high)

        self.hval_fold_label = QLabel(self)
        self.hval_fold_label.setText(str("{:3.2f}".format(self.set_fold_high * 0.01)))
        self.hval_fold_label.move(370, 110)

        self.low_fold_labal = QLabel(self)
        self.low_fold_labal.setText("Low Fold")
        self.low_fold_labal.move(20, 150)

        self.set_fold_low = int(self.folds[0] * 100)
        self.low_fold_slider = QSlider(Qt.Horizontal, self)
        self.low_fold_slider.setMinimumWidth(250)
        self.low_fold_slider.setMinimum(-100)
        self.low_fold_slider.setMaximum(100)
        self.low_fold_slider.setValue(self.set_fold_low)
        self.low_fold_slider.setSingleStep(1)
        self.low_fold_slider.move(110, 150)
        self.low_fold_slider.valueChanged.connect(changed_fold_low)

        self.lval_fold_label = QLabel(self)
        self.lval_fold_label.setText(str("{:3.2f}".format(self.set_fold_low * 0.01)))
        self.lval_fold_label.move(370, 150)

        self.phlaze_labal = QLabel(self)
        self.phlaze_labal.setText("Phlaze Rate")
        self.phlaze_labal.move(20, 190)

        self.set_phlaze = int(self.phlaze_speed[0] * 100)
        self.phlaze_slider = QSlider(Qt.Horizontal, self)
        self.phlaze_slider.setMinimumWidth(250)
        self.phlaze_slider.setMinimum(1)
        self.phlaze_slider.setMaximum(100)
        self.phlaze_slider.setValue(self.set_phlaze)
        self.phlaze_slider.setSingleStep(1)
        self.phlaze_slider.move(110, 190)
        self.phlaze_slider.valueChanged.connect(changed_phlaze)

        self.phlaze_val_label = QLabel(self)
        self.phlaze_val_label.setText(str("{:3.2f}".format(self.set_phlaze * 0.01)))
        self.phlaze_val_label.move(370, 190)

        self.phlaze_depth_labal = QLabel(self)
        self.phlaze_depth_labal.setText("Phlaze Depth")
        self.phlaze_depth_labal.move(20, 230)

        self.set_phlaze_depth = self.phlaze_depth[0]
        self.phlaze_depth_slider = QSlider(Qt.Horizontal, self)
        self.phlaze_depth_slider.setMinimumWidth(250)
        self.phlaze_depth_slider.setMinimum(1)
        self.phlaze_depth_slider.setMaximum(200)
        self.phlaze_depth_slider.setValue(self.set_phlaze_depth)
        self.phlaze_depth_slider.setSingleStep(1)
        self.phlaze_depth_slider.move(110, 230)
        self.phlaze_depth_slider.valueChanged.connect(changed_phlaze_depth)

        self.phlaze_depth_val_label = QLabel(self)
        self.phlaze_depth_val_label.setText(str(self.set_phlaze_depth))
        self.phlaze_depth_val_label.move(370, 230)

        self.phlaze_loops_labal = QLabel(self)
        self.phlaze_loops_labal.setText("Phlaze Loops")
        self.phlaze_loops_labal.move(20, 270)

        self.set_phlaze_loops = self.phlaze_loops[0]
        self.phlaze_loops_slider = QSlider(Qt.Horizontal, self)
        self.phlaze_loops_slider.setMinimumWidth(100)
        self.phlaze_loops_slider.setMinimum(1)
        self.phlaze_loops_slider.setMaximum(10)
        self.phlaze_loops_slider.setValue(self.set_phlaze_loops)
        self.phlaze_loops_slider.setSingleStep(1)
        self.phlaze_loops_slider.move(110, 270)
        self.phlaze_loops_slider.valueChanged.connect(changed_phlaze_loops)

        self.phlaze_loops_val_label = QLabel(self)
        self.phlaze_loops_val_label.setText(str(self.set_phlaze_loops))
        self.phlaze_loops_val_label.move(230, 270)

        self.phlaze_button = QPushButton(self)
        if self.phlazes[0]:
            self.phlaze_button.setText("Phlaze On")
        else:
            self.phlaze_button.setText("Phlaze Off")
        self.phlaze_button.setMinimumWidth(130)
        self.phlaze_button.move(30, 330)
        self.phlaze_button.clicked.connect(set_phlazes)
