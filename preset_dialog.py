#!/usr/bin/env python3

# Copyright (C) 2021 Harry Sentonbury
# GNU General Public License v3.0

from PyQt5.Qt import Qt
from PyQt5.QtWidgets import (QWidget, QLabel, QPushButton, QListWidget,
                             QSlider, QVBoxLayout, QLineEdit)

from PyQt5.QtGui import QIcon
import os
import pickle
import time


class PresetSaveDialog(QWidget):
    def __init__(self, settings_list):
        self.settings_list = settings_list
        super().__init__()


        def save_stuff():
            stamp = self.filename_entry.text()
            if len(stamp) == 0:
                stamp = "Presets-{}.pickle".format(str(time.ctime()
                                              [-16:].replace(" ", "-").replace(":", "-")))
            else:
                stamp = f"{stamp}.pickle"

            with open(f"presets/{stamp}", "wb+") as fp:
                pickle.dump(self.settings_list, fp)
            # print(f"pickled {self.settings_list}")
            self.close()


        self.setGeometry(200, 500, 600, 200)
        self.setWindowTitle("Save Presets")
        self.setWindowIcon(QIcon('images/scope-icon.jpg'))
        self.setObjectName("preset_win")

        self.save_labal = QLabel(self)
        self.save_labal.setText("Give Presets A Filename")
        self.save_labal.move(20, 10)

        self.filename_entry = QLineEdit(self)
        self.filename_entry.setMaxLength(32)
        self.filename_entry.move(20, 50)
        self.filename_entry.resize(280,40)

        self.button = QPushButton('Save', self)
        self.button.move(220,110)

        self.button.clicked.connect(save_stuff)


        self.close_button = QPushButton("Close", self)
        self.close_button.setMinimumWidth(130)
        self.close_button.move(400, 140)
        self.close_button.clicked.connect(lambda: self.close())


class PresetRecallDialog(QWidget):
    def __init__(self, all_that):
        self.all_that = all_that
        super().__init__()


        self.setGeometry(100, 320, 600, 600)
        self.setWindowTitle('Recall Preset File')
        self.setWindowIcon(QIcon('images/scope-icon.jpg'))
        self.setObjectName("p_recall_win")

        layout = QVBoxLayout()
        self.setLayout(layout)
        self.listwidget = QListWidget()
        for n, f in enumerate(sorted(os.listdir("./presets"))):
            self.listwidget.insertItem(n, f)
        self.listwidget.clicked.connect(self.clicked)
        layout.addWidget(self.listwidget)

        self.info_label = QLabel(self)
        self.info_label.setText("Select file and click")
        layout.addWidget(self.info_label)

        self.close_button = QPushButton("Close", self)
        self.close_button.clicked.connect(lambda: self.close())
        layout.addWidget(self.close_button)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Return:
            self.clicked(self)
        else:
            return


    def clicked(self, qmodelindex):
        pickle_item = self.listwidget.currentItem().text()
        try:
            with open(f"./presets/{pickle_item}", "rb") as fp:
                    preset_values = pickle.load(fp)
            # print(f"unpickled {preset_values}")
            self.all_that.freq_slider.setValue(preset_values[0])
            self.all_that.fm1_slider.setValue(preset_values[1])
            self.all_that.fm2_slider.setValue(preset_values[2])
            self.all_that.lfo_slider.setValue(preset_values[3])
            self.all_that.duration_slider.setValue(preset_values[4])
            self.all_that.lfo_amp_slider.setValue(preset_values[5])
            self.all_that.ramp_slider.setValue(preset_values[6])
            self.all_that.tremolo_slider.setValue(preset_values[7])
            self.all_that.volume_slider.setValue(preset_values[8])
            self.all_that.trem_amp_slider.setValue(preset_values[9])
            self.all_that.fmt_slider.setValue(preset_values[10])
            self.all_that.delay_slider.setValue(preset_values[11])
            self.all_that.fade_out_slider.setValue(preset_values[12])
            self.all_that.fade_in_slider.setValue(preset_values[13])
            self.all_that.noise_slider.setValue(preset_values[14])
            self.all_that.choose_lfo[0] = preset_values[15]         # int
            self.all_that.choose_fm2[0] = preset_values[16]     # bool
            self.all_that.choose_noise[0] = preset_values[17]   # int
            self.all_that.choose_wave[0] = preset_values[18]    # bool
            self.all_that.choose_trem[0] = preset_values[19]
            self.all_that.clippings[0] = preset_values[20]
            self.all_that.clippings[1] = preset_values[21]

            if self.all_that.choose_lfo[0] == 0:
                self.all_that.lfo_button.setText("ramp")
            elif self.all_that.choose_lfo[0] == 1:
                self.all_that.lfo_button.setText("LFO")
            elif self.all_that.choose_lfo[0] == 2:
                self.all_that.lfo_button.setText("LFO Clip")
            elif self.all_that.choose_lfo[0] == 3:
                self.all_that.lfo_button.setText("LFO Fold")
            else:
                self.all_that.lfo_button.setText("Sweep")

            if self.all_that.choose_fm2[0] is True:
                self.all_that.fm2_button.setText("FM2 On")
            else:
                self.all_that.fm2_button.setText("FM2 Off")

            if self.all_that.choose_noise[0] == 0:
                self.all_that.noise_button.setText("Noise Off")
            elif self.all_that.choose_noise[0] == 1:
                self.all_that.noise_button.setText("Noise >")
            elif self.all_that.choose_noise[0] == 2:
                self.all_that.noise_button.setText("Noise >")
            else:
                self.all_that.noise_button.setText("Noise ^")

            if self.all_that.choose_wave[0] is True:
                self.all_that.wave_button.setText('Triangle')
            else:
                self.all_that.wave_button.setText('Sin')

            if self.all_that.choose_trem[0] == 1:
                self.all_that.tremolo_sin[0] = True
                self.all_that.trem_button.setText("Tremolo Sin")
            elif self.all_that.choose_trem[0] == 2:
                self.all_that.tremolo_sin[0] = False
                self.all_that.trem_button.setText("Tremolo Cos")
            else:
                self.all_that.trem_button.setText("Tremolo Off")
            if len(preset_values) > 22:
                self.all_that.reverse_add[0] = preset_values[22]
                if self.all_that.reverse_add[0] == 1:
                    self.all_that.reverse_button.setText("And Reverse")
                elif self.all_that.reverse_add[0] == 2:
                    self.all_that.reverse_button.setText("Reversed")
                else:
                    self.all_that.reverse_button.setText("Not Reversed")
            if len(preset_values) > 23:
                self.all_that.foldings[0] = preset_values[23]
                self.all_that.foldings[1] = preset_values[24]
                self.all_that.phlazing[0] = preset_values[25]
                self.all_that.phlaze_speed[0] = preset_values[26]
            if len(preset_values) > 27:
                self.all_that.phlaze_loops[0] = preset_values[27]
                self.all_that.phlaze_depth[0] = preset_values[28]

        except Exception as e:
            print(f"{type(e).__name__}: {str(e)}")
        self.close()
