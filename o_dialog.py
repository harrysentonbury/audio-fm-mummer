# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0

import numpy as np
import sounddevice as sd
import threading

from PyQt5.Qt import Qt
from PyQt5.QtWidgets import (QWidget, QLabel, QListWidget, QPushButton,
                             QVBoxLayout, QSlider)

from PyQt5.QtGui import QIcon   # , QPixmap


class OutputDialog(QWidget):
    """ set blocksize """
    def __init__(self, streaming, default_blocksize, new_blocksize, output_device, stream_thread, stream_func, new_samplerate):
        self.streaming = streaming
        self.default_blocksize = default_blocksize
        self.new_blocksize = new_blocksize
        self.output_device = output_device
        self.stream_thread = stream_thread
        self.stream_func = stream_func
        self.new_samplerate = new_samplerate
        super().__init__()

        def stream_restart():
            self.streaming[0] = True
            self.stream_thread = threading.Thread(target=self.stream_func, args=[self.output_device[0],
                                             self.new_blocksize[0], self.new_samplerate[0]], daemon=True)
            self.stream_thread.start()


        def reset_default_func():
            self.new_blocksize[0] = self.default_blocksize
            self.output_device[0] = -1
            self.new_samplerate[0] = 48000
            close_devices_dialog()

        def close_devices_dialog():
            stream_restart()
            self.close()

        def changed_blocksize():
            new_blocksize[0] = int(2**self.blocksize_slider.value())
            self.blocksize_val_label.setText(f"Blocksize = {self.new_blocksize[0]}")
            self.blocksize_val_label.adjustSize()

        def changed_samplerate():
            new_samplerate[0] = self.samplerate_slider.value()
            if new_samplerate[0] == 0:
                new_samplerate[0] = 44100
                self.samplerate_val_label.setText(f"Samplerate = {new_samplerate[0]}")
            else:
                new_samplerate[0] = 48000
                self.samplerate_val_label.setText(f"Samplerate = {new_samplerate[0]}")



        self.streaming[0] = False
        query = repr(sd.query_devices())
        query = query.split('\n')
        self.setGeometry(240, 100, 500, 620)
        self.setWindowTitle('Set Output Device and Blocksize')
        self.setWindowIcon(QIcon('images/scope-icon.jpg'))
        self.setObjectName("op_win")

        layout = QVBoxLayout()
        self.setLayout(layout)
        self.listwidget = QListWidget()
        for i in range(len(query)):
            self.listwidget.insertItem(i, query[i])
        self.listwidget.clicked.connect(self.clicked)
        layout.addWidget(self.listwidget)

        self.blocksize_slider_label = QLabel(self)
        self.blocksize_slider_label.setText("Set Bocksize Slider")
        self.blocksize_slider_label.setMaximumWidth(200)
        self.blocksize_slider_label.setObjectName('bl_label')
        layout.addWidget(self.blocksize_slider_label)

        self.blocksize_slider = QSlider(Qt.Horizontal, self)
        self.blocksize_slider.setMaximumWidth(100)
        self.blocksize_slider.setMinimumHeight(20)
        self.blocksize_slider.setMinimum(6)
        self.blocksize_slider.setMaximum(11)
        self.blocksize_slider.setValue(int(np.log2(self.new_blocksize[0])))
        self.blocksize_slider.valueChanged.connect(changed_blocksize)
        layout.addWidget(self.blocksize_slider)

        self.blocksize_val_label = QLabel(self)
        #self.blocksize_val_label.setMinimumWidth(250)
        self.blocksize_val_label.setText(f'Blocksize = {self.new_blocksize[0]}')
        layout.addWidget(self.blocksize_val_label)

        self.samplerate_slider_label = QLabel(self)
        self.samplerate_slider_label.setText("Samplerate")
        self.samplerate_slider_label.setObjectName("sr_label")
        self.samplerate_slider_label.setMaximumWidth(200)
        layout.addWidget(self.samplerate_slider_label)

        self.samplerate_slider = QSlider(Qt.Horizontal, self)
        self.samplerate_slider.setMaximumWidth(50)
        self.samplerate_slider.setMinimum(0)
        self.samplerate_slider.setMaximum(1)
        self.samplerate_slider.setValue(1 if new_samplerate[0] == 48000 else 0)
        self.samplerate_slider.valueChanged.connect(changed_samplerate)
        layout.addWidget(self.samplerate_slider)

        self.samplerate_val_label = QLabel(self)
        self.samplerate_val_label.setText(f"Samplerate = {new_samplerate[0]}")
        layout.addWidget(self.samplerate_val_label)

        self.reset_button = QPushButton("reset Defaults")
        self.reset_button.setMaximumWidth(150)
        self.reset_button.clicked.connect(reset_default_func)
        layout.addWidget(self.reset_button)

        self.close_bsw_button = QPushButton("Select and Close", self)
        self.close_bsw_button.clicked.connect(close_devices_dialog)
        layout.addWidget(self.close_bsw_button)

    def clicked(self, qmodelindex):
        item = self.listwidget.currentItem()
        ploo = item.text()[:4].replace('*', '').strip()
        try:
            self.output_device[0] = int(ploo)
        except Exception as e:
            print(f"{type(e).__name__}: {str(e)}")
            self.output_device[0] = -1
