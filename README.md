# **audio-fm-mummer**

A thing for experimenting with audio frequency modulation with one or two
modulators. Sin or triangle. LFO, clipped LFO, folded LFO and log ramp. And a
few more odds and ends.

### python 3.7 or above

```
pip3 install numpy
pip3 install sounddevice
```
**Also PyQt5**

```
sudo apt-get install python3-pyqt5
```
git clone or download and extract.
cd into audio-fm-mummer directory:-

```
cd audio-fm-mummer
```

**Run**

```
python3 audio_fm_mummer.py
```

![sreenshot](images/mummer1.jpg)

Set sliders and / or buttons. click **Build** to build the waveform then
**Play**. Click **Loop** to play in a loop.
While it is looping **Buid** a different waveform and click **Add** and it will
play when the current one has finished and it will loop the new one, or click
**Play** to play it straight away. The sliders can be fine tuned with the arrow
keys. Tab and Space can be used to focus and press buttons and sliders.

The modulators frequency's are further modified by ramps or lfo as a function
of time.

The ramps are log base 10 of 0.0 to 1.0 (t start - t end) multiplied by
slider value. **FM2 Ramp Length** value is a ratio (total time:value).   
The noise is fade in or fade out, log base 5 of **Noise Shape**
value to 1.0. (more positive = more shallow curve).

The **Tremolo** will overflow into a ring modulator for values over 0.5, try a
setting of 7.0 at a slow speed (1 second), all will become clear.   
The Delay slider will delay the right channel by n seconds.

The **LFO Clip** values can be set with sliders in a dialogue accessed from the
drop down menu. Both high and low clipping values can range from -1.0 to 3.0.
The **LFO Fold** values can be set with sliders in a dialogue accessed from the
same drop down menu too. There is also a **Phlaze** button in there with
sliders. I suppose it will do for now.

![screenshot2](images/lfo_clip.jpg)

Output to any device Eg: USB audio interface. The dialogue with a list of
available audio devices to select from can be accessed from the dropdown menu.
**Settings > Set Output Device, Blocksize and Samplerate**. Click on the
required device from the list then click **Set / Close**. The blocksize and
samplerate 44100 or 48000 (default) can be adjusted with the sliders.
_Note_: If you close the dialogue using the **X** in the top right corner, the
stream will fail to restart. Reopen the dialogue, repeat your selection or
whatever and click **Set / Close** to restart the stream.

### Save and Recall Settings

To Save current settings **Settings > Save As Presets**. A dialogue will open.
A snapshot of the sliders and button settings is made the moment the dialogue
was opened. Give the presets a name and click **Save**. Close dialogue. The file
will be saved to the presets directory found in the audio-fm-mummer project
directory.

Recall presets **Settings > Recall Presets**. Click on the required
preset .pickle file from the list. the dialogue will close. Job done.
Arrow keys and enter key can also be used to select and apply preset.

### Keyboard Shortcuts

| Key    | Function                |
|:-------|:------------------------|
|  p     | Play                    |
|  b     | Build sound             |
|  a     | Add to loop             |
|  l     | Loop sound              |
|  s     | Stop                    |
|  t     | Tremolo                 |
|  f     | Toggle FM2              |
|  n     | Noise                   |
|  o     | Ramp, LFOs and shit     |
| Ctrl d | Open Clip Dialogue      |
| Ctrl o | Output Dialogue         |
| Ctrl s | Save Presets Dialogue   |
| Ctrl r | Recall Presets Dialogue |
| Ctrl q | Quit                    |
